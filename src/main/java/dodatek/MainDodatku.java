package dodatek;

import dodatek.equals_rowna_sie.ObiektEqualsRownaSie;

import java.util.*;

public class MainDodatku {

    public static void main(String[] args) {
//        equalsRownaSie();
//        zbiory();
//        mapy();
        listy();
    }

    public static void equalsRownaSie() {
        ObiektEqualsRownaSie obiekt1 = new ObiektEqualsRownaSie("Jan");
        ObiektEqualsRownaSie obiekt2 = new ObiektEqualsRownaSie("Jan");
        obiekt2 = obiekt1;

        boolean result1 = obiekt1.equals(obiekt2);
        boolean result2 = (obiekt1 == obiekt2);
        System.out.println("equals: " + result1);
        System.out.println("==: " + result2);
    }

    public static void zbiory() {
        Set<String> zbior = new HashSet<>();
        zbior.add("Jan");
        zbior.add("Alicja");
        zbior.add("Robert");
        zbior.add("Adam");
        zbior.add("Jan");
        zbior.add("Alicja");
        zbior.add("Jan");
        zbior.add("Alicja");
        zbior.add("sdfsdgdfggdfJan");
        zbior.add("dfgdfhdfh");
        zbior.add("hfhweyq45u");
        zbior.add("fghdfgjdfyjdfg");
        zbior.add("dasdwet4qg");
        zbior.add("Adfsgdfhfddasdwet4qg");

        ObiektEqualsRownaSie o = new ObiektEqualsRownaSie("Cos tam");
        o.wypiszZbior(zbior);

    }

    public static void mapy() {
        TreeMap<String, Integer> mapa = new TreeMap<>();
        mapa.put("Jan", 18);
        mapa.put("Jan", 30);
        mapa.put("Jan", 100);
        mapa.put("Alicja", 21);
        mapa.put("Robert", 55);
        mapa.put("Ktoś", 999);
        mapa.put("Alicja", 10);
        mapa.put("fsdjghs", 10);
        mapa.put("sadasfsd", 10);
        mapa.put("sdfsgdfg", 10);
        mapa.put("feryw5uw5", 10);
        mapa.put("Aasdasdsalicja", 10);
        mapa.put("q34tuyiAlicja", 10);
        mapa.put("sdgtrjr6tjAlicja", 10);

        ObiektEqualsRownaSie o = new ObiektEqualsRownaSie("Nieważne");
        o.wypiszMape(mapa);
    }

    public static void listy() {
        List<String> lista = new ArrayList<>();
        lista = new LinkedList<>();

        System.out.println(15 >> 1); // przesunięcie bitowe
    }
}
