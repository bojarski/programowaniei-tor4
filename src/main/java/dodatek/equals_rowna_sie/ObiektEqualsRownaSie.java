package dodatek.equals_rowna_sie;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class ObiektEqualsRownaSie {

    private String imie;

    public ObiektEqualsRownaSie(String imie) {
        this.imie = imie;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof ObiektEqualsRownaSie) {
            ObiektEqualsRownaSie innyObiekt = (ObiektEqualsRownaSie) obj;
            return innyObiekt.imie.equals(this.imie);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(imie);
    }

    public void wypiszZbior(Set<String> set) {
        System.out.println("Wypisz zbior");
        for (String s : set) {
            System.out.println(s);
        }
    }

    public void wypiszMape(Map<String, Integer> map) {
        System.out.println("Wypisz mapę");

        System.out.println("\nTylko po kluczach");
        for (String key : map.keySet()) {
            System.out.println(key);
        }

        System.out.println("\ntylko wartości");
        for (Integer i : map.values()) {
            System.out.println(i);
        }

        System.out.println("\npara klucz, wartość");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }
}
