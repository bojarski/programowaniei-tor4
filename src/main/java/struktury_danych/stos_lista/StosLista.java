package struktury_danych.stos_lista;

public class StosLista {

    private ElemStos top;

    public StosLista() {
        top = null;
    }

    public void push(int value) {
//        ElemStos newElem = new ElemStos(value, top);
//        top = newElem;

        top = new ElemStos(value, top);
    }

    public int pop() {
        if (isEmpty()) {
            throw new StosOutOfSizeException("Stos pusty!");
        }

        int value = top.getValue();
        top = top.getPrev();
        return value;
    }

    public int peek() {
        if (isEmpty()) {
            throw new StosOutOfSizeException("Stos pusty!");
        }

        return top.getValue();
    }

    public boolean isEmpty() {
//        if (top == null) {
//            return true;
//        } else {
//            return false;
//        }

        return top == null;
    }

    public void show() {
        ElemStos pointer = top;

        while (pointer != null) {
            System.out.print(pointer.getValue() + " ");
            pointer = pointer.getPrev();
        }
        System.out.println();
    }

    // ========================

    public static void main(String[] args) {
        StosLista stosLista = new StosLista();

        stosLista.push(1);
        stosLista.push(2);
        stosLista.push(3);

        stosLista.show();

        stosLista.pop();
//        stosLista.pop();

        stosLista.show();

        System.out.println("Peek: " + stosLista.peek());

        stosLista.show();

        try {
            stosLista.pop();
            stosLista.pop();
            stosLista.pop();
        } catch (StosOutOfSizeException e) {
            System.out.println(e.getMessage());
        }
    }

}
