package struktury_danych.stos_lista;

public class ElemStos {

    private int value;

    private ElemStos prev;

    public ElemStos(int value, ElemStos prev) {
        this.value = value;
        this.prev = prev;
    }

    public int getValue() {
        return value;
    }

    public ElemStos getPrev() {
        return prev;
    }
}
