package struktury_danych.stos_lista;

public class StosOutOfSizeException extends RuntimeException {

    public StosOutOfSizeException(String message) {
        super(message);
    }
}
