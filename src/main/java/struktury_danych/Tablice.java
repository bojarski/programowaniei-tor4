package struktury_danych;

import sun.tools.jconsole.Tab;

public class Tablice {

    public static void main(String[] args) {
        Tablice tablice = new Tablice();
        int[] tab = {4, 2, 3 ,5, 9, 8, 7, 10, 22, 143, 435, 2, 123, 333};

        tablice.liczbyOdKoncaNaParzystychIndeksach(tab);
        tablice.sumaLiczbPodzielnychPrzezTrzy(tab);
        tablice.sumaPieciuPierwszychOdejmujacOstatni(tab);
    }

    public void liczbyOdKoncaNaParzystychIndeksach(int[] tab) {
        for (int i = tab.length - 1; i >= 0; i--) {
            if(i % 2 == 0) {
                System.out.print(tab[i] + " ");
            }
        }
        System.out.println();
    }

    public void sumaLiczbPodzielnychPrzezTrzy(int[] tab) {
        int suma = 0;
        for (int i = 0; i <= tab.length - 1; i++) {
            if(tab[i] % 3 == 0) {
//                suma = suma + tab[i];
                suma += tab[i];
            }
        }
        System.out.println("Wynik sumaLiczbPodzielnychPrzezTrzy: " + suma);
    }

    public void sumaPieciuPierwszychOdejmujacOstatni(int[] tab) {
        if (tab.length < 6) {
            System.out.println("Za mała tablica");
            return;
        }

        int wynik = 0;
        for (int i = 0; i < 5; i++) {
            wynik += tab[i];
        }
//        wynik = wynik - tab[tab.length - 1];
        wynik -= tab[tab.length - 1];
        System.out.println("Wynik sumaPieciuPierwszychOdejmujacOstatni: " + wynik);
    }


}
