package struktury_danych.lista;

public class Lista {

    private ElemLista first;

    private ElemLista last;

    public Lista() {
        first = last = null;
    }

    public void addFirst(int value) {
        ElemLista newElem = new ElemLista(value);

        if(first == null) { // jeżeli lista pusta i dodajemy nowy element, to pierwszy element jest też ostatnim
//            first = last = newElem;
            first = newElem;
            last = newElem;
        } else { // w liście jest wiele elementów i dodajemy na początek nowy, zatem tylko FIRSTa modyfikujemy
            first.setPrev(newElem);
            newElem.setNext(first);
            first = newElem;
        }
    }

    public void addLast(int value) {
        ElemLista newElem = new ElemLista(value);

        if(last == null) {
            // first = last = newElem;
            first = newElem;
            last = newElem;
        } else {
            last.setNext(newElem);
            newElem.setPrev(last);
            last = newElem;
        }
    }

    public int peekFirst() {
        return first.getValue();
    }

    public int peekLast() {
        return last.getValue();
    }

    public int pollFirst() {
        int wartosc = 0;
        if (first == null) { // gdy lista pusta
            // TODO exception
        } else if(first.getNext() == null) { // gdy jeden element w liście
            wartosc = first.getValue();
            first = last = null;
        } else { // gdy wiele elementów w liście
            wartosc = first.getValue();
            first = first.getNext();
            first.setPrev(null);
        }

        return wartosc;
    }

    public int pollLast() {
        int wartosc = 0;
        if(last == null) { // gdy lista pusta
            //TODO exception
        } else if(last.getPrev() == null) { // gdy jeden element w liście
            wartosc = last.getValue();
            first = last = null;
        } else { // gdy wiele elementów
            wartosc = last.getValue();
            last = last.getPrev();
            last.setNext(null);
        }

        return wartosc;
    }

    public boolean isEmpty() {
        return first == null;
        // return last == null;
    }

    public void show() {
        ElemLista pointer = first;
        while (pointer != null) {
            System.out.print(pointer.getValue() + " ");
            pointer = pointer.getNext();
        }
        System.out.println();
    }

    public void showReverse() {
        ElemLista pointer = last;
        while (pointer != null) {
            System.out.print(pointer.getValue() + " ");
            pointer = pointer.getPrev();
        }
        System.out.println();
    }

    public ElemLista search(int value) {
        ElemLista pointer = first;
        while (pointer != null) {
            if(value == pointer.getValue()) {
                return pointer;
            }
            pointer = pointer.getNext();
        }
        return null;
    }

    public boolean remove(int value) {
        ElemLista found = search(value);

        if(found == null) {
            return false;
        }

        if (first == found) {
            pollFirst();
        } else if(last == found) {
            pollLast();
        } else { // element ze środka
            found.getPrev().setNext(found.getNext()); // ustawienie nexta dla poprzednika founda
            found.getNext().setPrev(found.getPrev()); // ustawienie poprzednika dla następnika founda
        }
        return true;
    }




    // ===========================

    public static void main(String[] args) {
        Lista lista = new Lista();

        lista.addFirst(1);
        lista.addFirst(2);
        lista.addFirst(3);
        lista.addLast(4);

        lista.show();
        lista.showReverse();

        lista.pollFirst();
        lista.show();

        lista.pollLast();
        lista.show();

        System.out.println("PeekFirst: " + lista.peekFirst());
        System.out.println("PeekLast: " + lista.peekLast());

        lista.show();
    }


}
