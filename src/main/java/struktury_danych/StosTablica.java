package struktury_danych;

import java.util.Scanner;

public class StosTablica {

    private int[] tab;
    private int top;

    public StosTablica(int rozmiar) {
        tab = new int[rozmiar];
        top = -1;
    }

    public void push(int wartosc) {
        if (isFull()) {
            throw new ArrayIndexOutOfBoundsException("Stos pełen!");
        }

//        top++;     // top = top + 1;
//        tab[top] = wartosc;

        tab[++top] = wartosc;
    }

    public int pop() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Stos pusty");
        }

//        int wartosc = tab[top];
//        top = top - 1;
//        return wartosc;

//        int wartosc = tab[top--];
//        return wartosc;

        return tab[top--];
    }

    public boolean isEmpty() {
//        if (top == -1) {
//            return true;
//        } else {
//            return false;
//        }

        return top == -1;
    }

    public boolean isFull() {
//        if (top + 1 == tab.length) {
//            return true;
//        } else {
//            return false;
//        }

        return top + 1 == tab.length;
    }

    public int peek() {
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Stos pusty");
        }

        return tab[top];
    }

    public void show() {
        for (int i = 0; i <= top; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }


    // ===========================

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj rozmiaru stosu:");
        int rozmiar = scanner.nextInt();
        StosTablica stosTablica = new StosTablica(rozmiar);

        while (true) {
            System.out.println("Podaj: 1 - push, 2 - pop, 3 - peek, 4 - isEmpty, 0 - wyjście");
            int operacje = scanner.nextInt();

            switch (operacje) {
                case 1:
                    System.out.println("Podaj wartość:");
                    int wartosc = scanner.nextInt();
                    try {
                        stosTablica.push(wartosc);
                    } catch (ArrayIndexOutOfBoundsException exceptionNazwa) {
                        System.out.println(exceptionNazwa.getMessage());
                    }
                    break;
                case 2:
                    try {
                        int wynikPop = stosTablica.pop();
                        System.out.println("Pop: " + wynikPop);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e.getMessage());
                    }

                    break;
                case 3:
                    try {
                        int wynikPeek = stosTablica.peek();
                        System.out.println("Peek: " + wynikPeek);
                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.println(e.getMessage());
                    }

                    break;
                case 4:
                    System.out.println("isEmpty: " + stosTablica.isEmpty());
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Niepoprawna operacja");
            }

            System.out.print("Stos: ");
            stosTablica.show();
        }

    }
}
