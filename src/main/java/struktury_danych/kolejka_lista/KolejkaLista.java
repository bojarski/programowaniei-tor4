package struktury_danych.kolejka_lista;

public class KolejkaLista {

    private ElemKolejka first;

    private ElemKolejka last;

    public KolejkaLista() {
        first = last = null;
    }

    public void add(int value) {
        ElemKolejka newElem = new ElemKolejka(value);

        if (first == null) { // gdy kolejka pusta wtedy pierwszy musi stać się ostatnim, bo zaczyna jeden element
            first = last = newElem;
        } else {
            last.setNext(newElem);
            last = newElem;
        }
    }

    public int poll() {
        if(isEmpty()) {
            // TODO exception
        }

        int value = first.getValue();
        first = first.getNext();

        if(first == null) { // kolejka zaczyna być pusta, zatem lasta musimy także zaktualizowac o null, bo nic nie ma w kolejce
            last = null;
        }

        return value;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public int peek() {
        if(isEmpty()) {
            // TODO exception
        }
        return first.getValue();
    }

    public void show() {
        ElemKolejka pointer = first;
        while (pointer != null) {
            System.out.print(pointer.getValue() + " ");
            pointer = pointer.getNext();
        }
        System.out.println();
    }


    // ==============================

    public static void main(String[] args) {
        KolejkaLista kolejkaLista = new KolejkaLista();

        kolejkaLista.add(1);
        kolejkaLista.add(2);
        kolejkaLista.add(3);

        kolejkaLista.show();

        kolejkaLista.poll();

        kolejkaLista.show();

        kolejkaLista.add(10);

        kolejkaLista.show();

        kolejkaLista.poll();

        kolejkaLista.show();

        System.out.println("Peek: " + kolejkaLista.peek());

        kolejkaLista.show();

        kolejkaLista.poll();
        kolejkaLista.poll();
        kolejkaLista.poll();

    }

}
