package struktury_danych.kolejka_lista;

public class ElemKolejka {

    private int value;

    private ElemKolejka next;

    public ElemKolejka(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ElemKolejka getNext() {
        return next;
    }

    public void setNext(ElemKolejka next) {
        this.next = next;
    }
}
