package spoj;

import java.util.Arrays;
import java.util.Scanner;

public class MainPrimeT {

    private static boolean[] sito;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        utworzSito(10000);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            int liczba = scanner.nextInt();

            if (czyPierwszaSito(liczba)) {
                System.out.println("TAK");
            } else {
                System.out.println("NIE");
            }

        }

    }

    public static void utworzSito(int rozmiar) {
        sito = new boolean[rozmiar + 1];
        Arrays.fill(sito, true);

        sito[0] = false;
        sito[1] = false;

        for (int i = 2; i * i <= sito.length; i++) {
            if (sito[i]) {
                for (int j = i * i; j < sito.length; j = j + i) {
                    sito[j] = false;
                }
            }
        }
    }

    public static boolean czyPierwszaSito(int n) {
        return sito[n];
    }

}
