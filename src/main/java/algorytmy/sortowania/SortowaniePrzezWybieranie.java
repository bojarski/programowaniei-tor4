package algorytmy.sortowania;

public class SortowaniePrzezWybieranie {

    public static void sortuj(int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            for (int j = i + 1; j < tab.length; j++) {
                if (tab[i] > tab[j]) {
                    TabUtils.swap(tab, i, j);
                }
            }
        }
    }
}
