package algorytmy.sortowania;

public class SortowanieSzybkie {

    // lewy - indeks początku podtablicy
    // prawy - indeks końca podtablicy
    public void sortuj(int[] tab, int lewyIndex, int prawyIndex) {
        if (lewyIndex >= prawyIndex) { // gdy podtablica będzie jednoelementowa, albo lewy minie się z prawym (warunek kończący rekurencję)
            return;
        }

        int pivotIndex = prawyIndex;
        int poz = lewyIndex; // jest pozycja, do wyliczenia, na której znajdzie się pivot

        for (int i = lewyIndex; i < prawyIndex; i++) {
            if (tab[i] < tab[pivotIndex]) {
//                TabUtils.swap(tab, i, poz);
//                poz++;
                TabUtils.swap(tab, i, poz++);
            }
        }

        TabUtils.swap(tab, pivotIndex, poz);
        sortuj(tab, lewyIndex, poz - 1); // wywołujemy rekurencyjnie dla lewej (z mniejszymi wartościami) podtablicy
        sortuj(tab, poz + 1, prawyIndex); // wywołujemy rekurencyjnie dla prawej (z większymi wartościami) podtablicy
    }

}
