package algorytmy.sortowania;

import java.util.Random;

public class MainSortowania {

    public static void main(String[] args) {
//        int[] tab = {10, 5, 2, 7, 1, 6, 3};

        int n = 100;
        int k = 10000000;
        int[] tab = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = random.nextInt(k);
        }
//        System.out.println("Tablica przed sortowanie: " + Arrays.toString(tab));

        long start;
        long stop;

        int[] tabPrzezWybieranie = tab.clone();
        int[] tabBabelkowe1 = tab.clone();
        int[] tabBabelkowe2 = tab.clone();
        int[] tabBabelkowe3 = tab.clone();
        int[] tabSzybkie = tab.clone();

        start = System.nanoTime();
        SortowaniePrzezWybieranie.sortuj(tabPrzezWybieranie);
        stop = System.nanoTime();
        System.out.println("Wybieranie: : " + (stop - start));
//        System.out.println("Wybieranie: " + Arrays.toString(tabPrzezWybieranie));

        start = System.nanoTime();
        SortowanieBabelkowe.sortuj1(tabBabelkowe1);
        stop = System.nanoTime();
        System.out.println("Babelkowe1: : " + (stop - start));
//        System.out.println("Babelkowe1: " + Arrays.toString(tabBabelkowe1));

        start = System.nanoTime();
        SortowanieBabelkowe.sortuj2(tabBabelkowe2);
        stop = System.nanoTime();
        System.out.println("Babelkowe2: : " + (stop - start));
//        System.out.println("Babelkowe2: " + Arrays.toString(tabBabelkowe2));

        start = System.nanoTime();
        SortowanieBabelkowe.sortuj3(tabBabelkowe3);
        stop = System.nanoTime();
        System.out.println("Babelkowe3: : " + (stop - start));
//        System.out.println("Babelkowe3: " + Arrays.toString(tabBabelkowe3));

        start = System.nanoTime();
        SortowaniePrzezZliczanie sortowaniePrzezZliczanie = new SortowaniePrzezZliczanie();
        int[] tabPrzezZliczanie = sortowaniePrzezZliczanie.sortuj(tab, k);
        stop = System.nanoTime();
        System.out.println("Zliczanie:  : " + (stop - start));
//        System.out.println("Zliczanie: " + Arrays.toString(tabPrzezZliczanie));

        start = System.nanoTime();
        SortowanieSzybkie sortowanieSzybkie = new SortowanieSzybkie();
        sortowanieSzybkie.sortuj(tabSzybkie, 0, tabSzybkie.length - 1);
        stop = System.nanoTime();
        System.out.println("Szybkie:    : " + (stop - start));
//        System.out.println("Szybkie: " + Arrays.toString(tabSzybkie));
    }
}
