package algorytmy.sortowania;

public class SortowanieBabelkowe {

    public static void sortuj1(int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            for (int j = 0; j < tab.length - 1; j++) {
                if (tab[j] > tab[j + 1]) {
                    TabUtils.swap(tab, j, j + 1);
                }
            }
        }
    }

    public static void sortuj2(int[] tab) {
        for (int i = 0; i < tab.length - 1; i++) {
            for (int j = 0; j < tab.length - 1 - i; j++) {
                if (tab[j] > tab[j + 1]) {
                    TabUtils.swap(tab, j, j + 1);
                }
            }
        }
    }

    public static void sortuj3(int[] tab) {
        boolean czyZamienilesJakieLiczby = true;
        for (int i = 0; i < tab.length - 1 && czyZamienilesJakieLiczby; i++) {
            czyZamienilesJakieLiczby = false;
            for (int j = 0; j < tab.length - 1 - i; j++) {
                if (tab[j] > tab[j + 1]) {
                    TabUtils.swap(tab, j, j + 1);
                    czyZamienilesJakieLiczby = true;
                }
            }

//            if(!czyZamienilesJakieLiczby) { // przerzcuono do for i
//                break;
//            }
        }
    }


}
