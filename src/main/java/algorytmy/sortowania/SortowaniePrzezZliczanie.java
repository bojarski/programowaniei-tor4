package algorytmy.sortowania;

public class SortowaniePrzezZliczanie {

    private int[] countingTab;

    private int[] resultTab;

    public int[] sortuj(int[] tab, int maxNum) { // maxNum - największa wartość w tablicy
        countingTab = new int[maxNum + 1];
        resultTab = new int[tab.length];

//        Arrays.fill(countingTab, 0); // domyślnie jest wyzerowana
        for (int i = 0; i < tab.length; i++) { // zliczamy elementy
            countingTab[tab[i]]++;
        }

        for (int i = 1; i < countingTab.length; i++) { // zliczamy ile jest elementów poprzedzających z daną liczbą
//            countingTab[i] = countingTab[i] + countingTab[i - 1];
            countingTab[i] += countingTab[i - 1];
        }

        for (int i = 0; i < tab.length; i++) { // przypisanie wartość z tablicy wejściowej (tab) na odpowiednim indeksie w resultTab[]
            resultTab[--countingTab[tab[i]]] = tab[i];
        }

        return resultTab;
    }

}
