package algorytmy.bst;

import java.util.LinkedList;
import java.util.Queue;

public class BinarneDrzewoPrzeszukiwan {

    private Wezel korzen;

    public BinarneDrzewoPrzeszukiwan() {
        this.korzen = null;
    }

    public static void main(String[] args) {
        BinarneDrzewoPrzeszukiwan bst = new BinarneDrzewoPrzeszukiwan();
        bst.dodaj(4);
        bst.dodaj(2);
        bst.dodaj(7);
        bst.dodaj(10);
        bst.dodaj(3);
        bst.dodaj(6);
        bst.dodaj(15);
        bst.dodaj(21);
        bst.dodaj(1);
        bst.dodaj(5);

        bst.usunRekurencyjnie(4, bst.getKorzen());

        System.out.print("BFS: ");
        bst.bfs();
        System.out.print("\npreorder: ");
        bst.preorder(bst.getKorzen());
        System.out.print("\ninorder: ");
        bst.inorder(bst.getKorzen());
        System.out.print("\npostorder: ");
        bst.postorder(bst.getKorzen());
    }

    public void dodaj(int wartosc) {
        korzen = dodajRekurencyjnie(wartosc, korzen);
    }

    private Wezel dodajRekurencyjnie(int wartosc, Wezel biezacy) {
        if (biezacy == null) { // tworzymy nową wartość
            return new Wezel(wartosc);
        }

        if (wartosc < biezacy.getWartosc()) { // idziemy w lewo, bo mniejszy
            biezacy.setLewy(dodajRekurencyjnie(wartosc, biezacy.getLewy()));
        } else if (wartosc >= biezacy.getWartosc()) { // idziemy w prawo, bo większy bądź równy
            biezacy.setPrawy(dodajRekurencyjnie(wartosc, biezacy.getPrawy()));
        }
        return biezacy;
    }

    public boolean znajdz(int wartosc) {
        return znajdzRekurencyjnie(wartosc, korzen);
    }

    private boolean znajdzRekurencyjnie(int wartosc, Wezel biezacy) {
        if (biezacy == null) {
            return false;
        }

        if (wartosc == biezacy.getWartosc()) {
            return true;
        } else if (wartosc < biezacy.getWartosc()) {
            return znajdzRekurencyjnie(wartosc, biezacy.getLewy());
        } else { // wartosc > biezacy.getWartosc()
            return znajdzRekurencyjnie(wartosc, biezacy.getPrawy());
        }

    }

    private Wezel usunRekurencyjnie(int wartosc, Wezel biezacy) {
        if (biezacy == null) {
            return biezacy;
        }

        if (wartosc == biezacy.getWartosc()) {
            if (biezacy.getLewy() == null && biezacy.getPrawy() == null) { // brak dziecka lewego i prawego
                return null;
            } else if (biezacy.getPrawy() == null) { // posiada TYLKO lewe dziecko
                return biezacy.getLewy();
            } else if (biezacy.getLewy() == null) { // posiada TYLKO prawe dziecko
                return biezacy.getPrawy();
            } else { // posiada dokładnie DWOJE dzieci
                int najmniejszaWartosc = znajdzWartoscNajmniejszaRekurencyjnie(biezacy.getPrawy());
                biezacy.setWartosc(najmniejszaWartosc);
                biezacy.setPrawy(usunRekurencyjnie(najmniejszaWartosc, biezacy.getPrawy()));
            }

        } else if (wartosc < biezacy.getWartosc()) {
            biezacy.setLewy(usunRekurencyjnie(wartosc, biezacy.getLewy()));
        } else { // wartosc > biezacy.getWartosc()
            biezacy.setPrawy(usunRekurencyjnie(wartosc, biezacy.getPrawy()));
        }
        return biezacy;
    }

    private int znajdzWartoscNajmniejsza(Wezel biezacy) {
        while (biezacy.getLewy() != null) {
            biezacy = biezacy.getLewy();
        }
        return biezacy.getWartosc();
    }

    private int znajdzWartoscNajmniejszaRekurencyjnie(Wezel biezacy) {
        if (biezacy.getLewy() == null) { // dochodzimy na sam dół lewego poddrzewa
            return biezacy.getWartosc();
        }
        return znajdzWartoscNajmniejszaRekurencyjnie(biezacy.getLewy());
    }

    public void bfs() {
        if (korzen == null) {
            return;
        }

        Queue<Wezel> queue = new LinkedList<>();
        queue.add(korzen);

        while (!queue.isEmpty()) {
            Wezel wezel = queue.poll();
            int wartosc = wezel.getWartosc();
            System.out.print(wartosc + " ");

            if (wezel.getLewy() != null) {
                queue.add(wezel.getLewy());
            }
            if (wezel.getPrawy() != null) {
                queue.add(wezel.getPrawy());
            }
        }
    }

    public void preorder(Wezel wezel) {
        if (wezel != null) {
            System.out.print(wezel.getWartosc() + " ");
            preorder(wezel.getLewy());
            preorder(wezel.getPrawy());
        }
    }

    public void inorder(Wezel wezel) {
        if (wezel != null) {
            inorder(wezel.getLewy());
            System.out.print(wezel.getWartosc() + " ");
            inorder(wezel.getPrawy());
        }
    }

    public void postorder(Wezel wezel) {
        if (wezel != null) {
            postorder(wezel.getLewy());
            postorder(wezel.getPrawy());
            System.out.print(wezel.getWartosc() + " ");
        }
    }

    // =====================================

    public Wezel getKorzen() {
        return korzen;
    }


}
