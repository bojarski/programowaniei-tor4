package algorytmy;

public class BinarySearch {

    public void oblicz(int[] A, int y) {
        int indeks = -1;
        int lewo = 0;
        int prawo = A.length - 1;

        while (lewo < prawo) {
            int srodek = (lewo + prawo) / 2;

            if (A[srodek] < y) {
                lewo = srodek + 1;
            } else {
                prawo = srodek;
            }
        }

        if (A[lewo] == y) {
            indeks = lewo;
            System.out.println("Znaleziono na indeksie: " + indeks);
        } else {
            indeks = -1;
            System.out.println("Nie znaleziono");
        }
    }

    // =============================

    public static void main(String[] args) {
        int[] tabica = {2, 4, 7, 12, 50, 51, 67, 99, 102};
        int szukanaLiczba = 99;

        new BinarySearch().oblicz(tabica, szukanaLiczba);
    }
}
