package algorytmy.dijkstra;

import java.util.Comparator;

public class VertexComparator implements Comparator<Vertex> {

    @Override
    public int compare(Vertex o1, Vertex o2) {
//        if(o1.getWeight() < o2.getWeight()) {
//            return -1;
//        } else if(o1.getWeight() == o2.getWeight()) {
//            return 0;
//        } else {
//            return 1;
//        }

//        return Integer.compare(o1.getWeight(), o2.getWeight());

        return o1.getWeight() - o2.getWeight();
    }
}
