package algorytmy.dijkstra;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Dijkstra {

    private boolean[] visitedTab; // czy wierzchołek został już przetworzony

    private int[] distanceTab; // odległość od wierzchołka początkowego/źródłowego

    private int[] sourceTab; // z jakiego wierzchołka przyszliśmy

    private LinkedList<Vertex>[] adjacentList; // lista sąsiedztwa

    private int verticesNum; // ilość wierzchołków

    private PriorityQueue<Vertex> priorityQueue; // kolejka piorytetowa

    public Dijkstra(int verticesNum, LinkedList<Vertex>[] adjacentList) {
        this.verticesNum = verticesNum;
        this.adjacentList = adjacentList;

        visitedTab = new boolean[verticesNum];
        distanceTab = new int[verticesNum];
        sourceTab = new int[verticesNum];

        priorityQueue = new PriorityQueue<>(new VertexComparator());
    }

    public static void main(String[] args) {
        int vNum; // ilość wierzchołków
        int eNum; // ilość krawędzi
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj ilość wierzchołków");
        vNum = scanner.nextInt();

        System.out.println("Podaj ilość krawędzi");
        eNum = scanner.nextInt();

        LinkedList<Vertex>[] tabList = new LinkedList[vNum]; // tablica list
        for (int i = 0; i < vNum; i++) {
            tabList[i] = new LinkedList<>();
        }

        System.out.println("Podaj krawędź w postaci (odzielone spacją): źródło cel waga");
        for (int i = 0; i < eNum; i++) {
            int source = scanner.nextInt();
            int destination = scanner.nextInt();
            int weight = scanner.nextInt();

            tabList[source].add(new Vertex(destination, weight));

            // informacyjnie
//            int[] tab;
//
//            ArrayList<Vertex> arrayList = new ArrayList<>();
//            arrayList.add(new Vertex(destination, weight) );
        }

        Dijkstra dijkstra = new Dijkstra(vNum, tabList);
        dijkstra.calculateMinPath(1);
        dijkstra.print();

    }

    public void calculateMinPath(int source) { // wierzchołek źródłowy, od którego liczymy wszystkie pozostałe ścieżki
        for (int i = 0; i < verticesNum; i++) {
            distanceTab[i] = Integer.MAX_VALUE;
            sourceTab[i] = -1;
//            visitedTab[i] = false;
        }
        // alternatywa do powyższego
//        Arrays.fill(distanceTab, Integer.MAX_VALUE);
//        Arrays.fill(sourceTab, -1);

        // ustalenie wartości początkowych
        distanceTab[source] = 0;
        sourceTab[source] = source;

        priorityQueue.add(new Vertex(source, 0));

        while (!priorityQueue.isEmpty()) {
            Vertex vSource = priorityQueue.poll(); // vertex
            int idSource = vSource.getId(); // id
            int distanceSource = vSource.getWeight();

            if (visitedTab[idSource]) { // jeżeli już odwiedzony to kontynuuj
                continue;
            }
            // jeżeli nie odwiedzony to zaznacz, że odwiedzony i przetwarzaj dalej
            visitedTab[idSource] = true;

//            for (int i = 0; i < adjacentList[idSource].size(); i++) {
//                Vertex vDestination = adjacentList[idSource].get(i);
//                int idDestination = vDestination.getId();
//                int wDestination = vDestination.getWeight(); // weight
//            }
            //alternatywa do powyższego
            for (Vertex vDestination : adjacentList[idSource]) {
                int idDestination = vDestination.getId();
                int wDestination = vDestination.getWeight(); // weight

                if (distanceTab[idSource] + wDestination < distanceTab[idDestination]) {
                    distanceTab[idDestination] = distanceTab[idSource] + wDestination; // aktualizacja ścieżki o mniejszą wartość
                    sourceTab[idDestination] = idSource;
                }
                priorityQueue.add(new Vertex(idDestination, distanceTab[idDestination]));
            }
        }
    }

    // =================================

    public void print() {
        for (int i = 0; i < verticesNum; i++) {
            System.out.printf("%d -> %d: %d\n", sourceTab[i], i, distanceTab[i]);
        }
    }

}
