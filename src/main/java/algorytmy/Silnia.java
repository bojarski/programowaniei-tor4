package algorytmy;

public class Silnia {

    public static void main(String[] args) {
        Silnia silnia = new Silnia();
        int wynik = silnia.rekurencyjnie(5);
        System.out.println("Silnia: " + wynik);
    }

    public int rekurencyjnie(int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 1;
        }

        System.out.println("Przed rekurencją dla : " + n);
        int wynikRekurencyjnie = rekurencyjnie(n - 1);
        int wynikCaly = n * wynikRekurencyjnie;
        System.out.println("Po rekurencji dla : " + n + " wynikCaly: " + wynikCaly + " wynikRek: " + wynikRekurencyjnie);

        return wynikCaly;
    }

}
