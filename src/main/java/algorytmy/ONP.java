package algorytmy;

import struktury_danych.stos_lista.StosLista;

import java.util.Scanner;

public class ONP {

    public ONP() {

    }

    public void uruchom() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wyrażenie w Odwrotnej Notacji Polskiej:");

        String wyrazenie = scanner.nextLine();

        String[] podzielone = wyrazenie.split(" ");
        StosLista stos = new StosLista();

        for(String element : podzielone) {

            try {
                int wartosc = Integer.parseInt(element);
                stos.push(wartosc);
            } catch (NumberFormatException e) {
                int drugiArgument = stos.pop();
                int pierwszyArgument = stos.pop();

                int wynik = 0;

                switch (element) {
                    case "+":
                        wynik = pierwszyArgument + drugiArgument;
                        break;
                    case "-":
                        wynik = pierwszyArgument - drugiArgument;
                        break;
                    case "*":
                        wynik = pierwszyArgument * drugiArgument;
                        break;
                    case "/":
                        wynik = pierwszyArgument / drugiArgument;
                        break;
                    default:
                        System.out.println("Niepoprawny znak operacji");
                }

                stos.push(wynik);
            }

        }

        int ostatecznyWynik = stos.pop();
        System.out.println("Wynik operacji to: " + ostatecznyWynik);

    }


    public static void main(String[] args) {
        new ONP().uruchom();
    }

}
