package algorytmy;

public class NWW { // Najmniejsza Wspólna Wielokrotność

    public static int oblicz(int a, int b) {
        return a * b / NWD.przezDzielenie(a, b);
    }

    public static void main(String[] args) {
        int wynik = NWW.oblicz(2, 3);
        System.out.println("NWW: " + wynik);
//        System.out.println("NWW: " + NWW.oblicz(2, 3));
    }

}
