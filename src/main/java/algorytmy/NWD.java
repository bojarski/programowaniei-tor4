package algorytmy;

public class NWD {

    public static int przezOdejmowanie(int a, int b) {
        while (b != 0) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    public static int przezDzielenie(int a, int b) {
        while (b != 0) {
            int r = a % b;
            a = b;
            b = r;
        }
        return a;
    }


    // =========================

    public static void main(String[] args) {
        int a = 6;
        int b = 8;
        int wynik = NWD.przezOdejmowanie(a, b);
        System.out.printf("NWD przez odejmowanie(%d, %d) = %d\n", a, b, wynik);

        System.out.printf("NWD przez dzielenie(%d, %d) = %d\n", a, b, NWD.przezDzielenie(a, b));
    }
}
