package algorytmy;

import java.util.Arrays;

public class LiczbyPierwsze {

    private static boolean[] sito;

    public static boolean czyPierwsza(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean czyPierwszaUlepszony(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i * i <= n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }


    public static void utworzSito(int rozmiar) {
        sito = new boolean[rozmiar + 1];
        Arrays.fill(sito, true);

        sito[0] = false;
        sito[1] = false;

        for (int i = 2; i * i <= sito.length; i++) {
            if (sito[i]) {
                for (int j = i * i; j < sito.length; j = j + i) {
                    sito[j] = false;
                }
            }
        }
    }

    public static boolean czyPierwszaSito(int n) {
        return sito[n];
    }

    public static void wypiszSito() {
        for (int i = 0; i < sito.length; i++) {
            if (sito[i]) {
                System.out.print(i + ", ");
            }
        }
        System.out.println();
    }


    public static void main(String[] args) {
        int rozmiar = 10000000;

//        LiczbyPierwsze.wypiszSito();

        int n = 9999991;
        long start;
        long stop;

        start = System.nanoTime();
        czyPierwsza(n);
        stop = System.nanoTime();
        System.out.println("Czy pierwsza zwykła czas: " + (stop - start));


        start = System.nanoTime();
        czyPierwszaUlepszony(n);
        stop = System.nanoTime();
        System.out.println("Czy pierwsza Ulepszona zwykła czas: " + (stop - start));

        start = System.nanoTime();
        LiczbyPierwsze.utworzSito(rozmiar);
        czyPierwszaSito(n);
        stop = System.nanoTime();
        System.out.println("Czy pierwsza Sito czas: " + (stop - start));

    }
}
